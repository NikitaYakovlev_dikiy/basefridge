package ru.webant.base.core.network.models

/**
 * Created by Dast_6_Tino on 11.09.18.
 */
data class LoginModel(
    var access_token: String,
    var refresh_token: String
)
package ru.webant.base.core.network.models

import java.util.*
import kotlin.math.max

/**
 * Created by Dast_6_Tino on 20.02.19.
 */
data class ListResponse<T>(
    var items: ArrayList<T>,
    var itemsPerPage: Int? = null,
    var totalItems: Int? = null,
    var countOfPages: Int? = null,
    var currentPage: Int? = null
) {

    fun currentProgress() =
        if (totalItems == null || totalItems == 0) {
            0F
        } else {
            max(
                items.size.toFloat() / totalItems!!.toFloat(),
                (currentPage?.toFloat() ?: 0F) / (countOfPages?.toFloat() ?: 1F)
            )
        }

    fun currentPage() =
        if (itemsPerPage == null || itemsPerPage == 0) {
            0
        } else {
            Math.ceil(items.size.toDouble() / itemsPerPage!!).toInt()
        }

    fun nextPage() =
        currentPage() + 1

    fun addList(newList: ListResponse<T>) {
        itemsPerPage = newList.itemsPerPage
        totalItems = newList.totalItems
        countOfPages = newList.countOfPages
        items.addAll(newList.items)
    }
}
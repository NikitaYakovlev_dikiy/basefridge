package ru.webant.base.core

fun <T> ifAny(vararg value: T, predicate: (T) -> Boolean): Boolean {
    var isOk = true
    value.forEach {
        if (!predicate.invoke(it)) {
            isOk = false
        }
    }
    return isOk
}
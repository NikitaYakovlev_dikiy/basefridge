package ru.webant.base.core.network

import java.util.*

/**
 * Created by Dast_6_Tino on 20.02.19.
 */
object NETWORK_CONST {
    const val ERROR_400 = "400"
    const val ERROR_404 = "404"
    const val ERROR_409 = "409"
    /**
     * 413 Request Entity Too Large
     */
    const val ERROR_413 = "413"
    const val ERROR_429 = "429"
    const val ERROR_500 = "500"

}

object REQUEST_CONST {
    const val GRANT_TYPE_PASSWORD = "password"
    const val GRANT_TYPE_REFRESH_TOKEN = "refresh_token"

    const val NO_SUCH_ELEMENT_EXCEPTION = "NoSuchElementException"
    const val UNABLE_TO_RESOLVE_HOST = "Unable to resolve host"
    const val TIMEOUT = "timeout"
    const val INVALID_USERNAME = "Invalid username"

    val CLIENT_NAME = "Android ${UUID.randomUUID()}"
}
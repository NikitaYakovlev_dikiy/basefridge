package ru.webant.base.core.shared

import android.content.Context

abstract class BasePreferenceWorker(context: Context) {

    private val preferences =
        context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)

    var accessToken = PreferenceValue<String>("access_token").value
    var refreshToken = PreferenceValue<String>("refresh_token").value
    var clientSecret = PreferenceValue<String>("client_secret").value
    var clientId = PreferenceValue<String>("client_id").value
    var deviceId = PreferenceValue("device_id", convertToValue = { it.toInt() }).value

    fun cleanAuthorizationData() {
        accessToken = null
        refreshToken = null
    }

    inner class PreferenceValue<T>(
        private val key: String,
        private val convertToValue: ((value: String) -> T)? = null,
        private val convertToPreference: ((value: T) -> String)? = null
    ) {
        var value
            get() = get()
            set(value) = set(value)

        fun set(value: T?) {
            val editor = preferences.edit()
            if (value != null) {
                editor.putString(key, convertToPreference?.invoke(value) ?: value.toString())
            } else {
                editor.putString(key, null)
            }
            editor.apply()
        }

        fun get(): T? {
            val value = preferences.getString(key, null) ?: return null
            @Suppress("UNCHECKED_CAST")
            return convertToValue?.invoke(value) ?: (value as? T)
        }
    }
}
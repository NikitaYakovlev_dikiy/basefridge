package ru.webant.base.core.network.pagination

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.webant.base.core.network.models.ListResponse
import ru.webant.base.ui.adapter.PaginationGateway

class PaginationSourse<T>(
    items: ArrayList<T>,
    private val requestNext: (nextPage: Int) -> Single<ListResponse<T>>
) {
    private var listResponse = ListResponse(items)
    private val composeDisposable = CompositeDisposable()
    private var onUpdate: ((progress: Float) -> Unit)? = null
    private var onError: ((Throwable) -> Unit)? = null
    private var paginationGateway: PaginationGateway<T>? = null

    constructor(
        view: PaginationGateway<T>,
        requestNext: (nextPage: Int) -> Single<ListResponse<T>>
    ) : this(view.linkItems(), requestNext) {
        this.paginationGateway = view
        listResponse = ListResponse(paginationGateway!!.linkItems())
        paginationGateway!!.requestNext {
            requestNextPage()
        }
    }

    fun doOnError(doOnError: (Throwable) -> Unit): PaginationSourse<T> {
        onError = doOnError
        return this
    }

    fun doOnUpdate(doOnUpdate: (progress: Float) -> Unit): PaginationSourse<T> {
        onUpdate = doOnUpdate
        return this
    }

    fun requestNextPage() {
        composeDisposable.clear()
        val page = listResponse.nextPage()
        if (page <= listResponse.countOfPages ?: Int.MAX_VALUE)
            requestNext.invoke(page)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    listResponse.addList(it)
                    paginationGateway?.onUpdate(listResponse.currentProgress())
                    onUpdate?.invoke(listResponse.currentProgress())
                }, {
                    paginationGateway?.onError(it)
                    onError?.invoke(it)
                })
                .addToComposite()
    }

    fun refresh() {
        composeDisposable.clear()
        listResponse.items.clear()
        paginationGateway?.onUpdate(listResponse.currentProgress())
        onUpdate?.invoke(listResponse.currentProgress())
    }

    private fun Disposable.addToComposite() {
        composeDisposable.add(this)
    }
}
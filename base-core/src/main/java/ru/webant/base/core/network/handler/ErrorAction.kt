package ru.webant.base.core.network.handler

class ErrorAction(
    var regexString: String,
    var action: (String) -> Unit
) {
    var regex = Regex(regexString)
}
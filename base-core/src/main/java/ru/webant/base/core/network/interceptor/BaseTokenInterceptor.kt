package ru.webant.base.core.network.interceptor

import okhttp3.*
import ru.webant.base.core.shared.BasePreferenceWorker

class BaseTokenInterceptor(var preference: BasePreferenceWorker) : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response? {
        val token = preference.accessToken
        val request = chain?.request()

        return try {
            if (!token.isNullOrEmpty()) {
                chain?.proceed(
                    request
                        ?.newBuilder()
                        ?.addHeader("Authorization", "Bearer $token")
                        ?.addHeader("Accept", "*/*")
                        ?.build()!!
                )
            } else {
                chain?.proceed(request!!)
            }
        } catch (e: Exception) {
            val message = e.message
            e.printStackTrace()

            Response.Builder()
                .body(
                    ResponseBody.create(
                        MediaType.parse("-"),
                        message ?: ""
                    )
                )
                .protocol(Protocol.HTTP_2)
                .code(404)
                .request(request!!)
                .message(message ?: "")
                .build()
        }
    }
}

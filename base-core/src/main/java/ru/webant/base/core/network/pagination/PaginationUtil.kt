package ru.webant.base.core.network.pagination

import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.webant.base.core.network.models.ListResponse

fun <T> getAll(
    limit: Int? = null,
    requestNext: (nextPage: Int) -> Single<ListResponse<T>>
): Flowable<ListResponse<T>> {
    var isEnd = false
    var nextPage = 1
    return Single.defer {
        return@defer requestNext.invoke(nextPage)
            .map {
                it.currentPage = nextPage
                nextPage++
                isEnd = it.countOfPages ?: 0 < nextPage
                it
            }
    }
        .subscribeOn(Schedulers.io())
        .unsubscribeOn(Schedulers.single())
        .observeOn(AndroidSchedulers.mainThread())
        .repeatUntil { isEnd }

}
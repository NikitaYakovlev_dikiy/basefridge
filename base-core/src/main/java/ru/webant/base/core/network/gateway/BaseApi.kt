package ru.webant.base.core.network.gateway

import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST
import ru.webant.base.core.network.models.LoginModel
import ru.webant.base.core.network.REQUEST_CONST.GRANT_TYPE_PASSWORD
import ru.webant.base.core.network.REQUEST_CONST.GRANT_TYPE_REFRESH_TOKEN

/**
 * Created by Dast_6_Tino on 20.02.19.
 */
interface BaseApi {
    @POST("/oauth/v2/token")
    @Headers("Accept: application/json, */*")
    @FormUrlEncoded
    fun getToken(
        @Field("client_id") client_id: String,
        @Field("client_secret") client_secret: String,
        @Field("username") username: String? = null,
        @Field("password") password: String? = null,
        @Field("grant_type") grant_type: String = GRANT_TYPE_PASSWORD
    ): Single<LoginModel>

    @POST("/oauth/v2/token")
    @Headers("Accept: application/json, */*")
    @FormUrlEncoded
    fun getTokenByRefresh(
        @Field("client_id") client_id: String,
        @Field("client_secret") client_secret: String,
        @Field("refresh_token") refreshToken: String,
        @Field("grant_type") grant_type: String = GRANT_TYPE_REFRESH_TOKEN
    ): Single<LoginModel>

}
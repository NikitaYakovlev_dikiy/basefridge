package ru.webant.base.core.network.handler

interface BaseErrorGateway {

    fun errorUnidentified(message: String?) {
        anyError(message ?: "")
    }

    fun error3xx(message: String) {
        anyError(message)
    }

    fun error404(message: String) {
        anyError(message)
    }

    fun error409(message: String) {
        anyError(message)
    }

    fun error413(message: String) {
        anyError(message)
    }

    fun error429(message: String) {
        anyError(message)
    }

    fun error4xx(message: String) {
        anyError(message)
    }

    fun error5xx(message: String) {
        anyError(message)
    }

    fun anyError(message: String)
}
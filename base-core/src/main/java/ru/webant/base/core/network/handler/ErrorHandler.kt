package ru.webant.base.core.network.handler

class ErrorHandler(
    private val errorGateway: BaseErrorGateway,
    private var customError: ArrayList<ErrorAction>? = null
) : (Throwable) -> Unit {

    override fun invoke(t: Throwable) {
        val message = t.message

        val regex3xx = Regex("HTTP 3\\d*")

        val regex404 = Regex("HTTP 404")
        val regex409 = Regex("HTTP 409")
        val regex413 = Regex("HTTP 413")
        val regex429 = Regex("HTTP 429")
        val regex4xx = Regex("HTTP 4\\d*")

        val regex5xx = Regex("HTTP 5\\d*")

        val customAction = customError?.firstOrNull {
            message?.contains(it.regex) == true
        }?.action

        when {
            message == null -> errorGateway.errorUnidentified(message)

            customAction != null -> customAction.invoke(message)

            message.contains(regex3xx) -> errorGateway.error3xx(message)

            message.contains(regex404) -> errorGateway.error404(message)
            message.contains(regex409) -> errorGateway.error409(message)
            message.contains(regex413) -> errorGateway.error413(message)
            message.contains(regex429) -> errorGateway.error429(message)
            message.contains(regex4xx) -> errorGateway.error4xx(message)

            message.contains(regex5xx) -> errorGateway.error5xx(message)

            else -> errorGateway.errorUnidentified(message)
        }
    }

}
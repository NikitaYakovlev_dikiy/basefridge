package ru.webant.base.core.network.interceptor

import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import ru.webant.base.core.ifAny
import ru.webant.base.core.network.gateway.BaseApi
import ru.webant.base.core.shared.BasePreferenceWorker
import java.io.IOException


abstract class BaseAuthenticator(var preference: BasePreferenceWorker) : Authenticator {

    abstract val baseApi: BaseApi?
    abstract var tokenHasExpired: () -> Unit


    private fun requestNewAccessToken(): String? {
        val id = preference.clientId
        val secret = preference.clientSecret
        val token = preference.refreshToken
        return if (ifAny(id, secret, token, baseApi) { it != null }) {
            baseApi!!.getTokenByRefresh(id!!, secret!!, token!!)
                .blockingGet()
                .let { model ->
                    preference.accessToken = model.access_token
                    preference.refreshToken = model.refresh_token
                    model.access_token
                }
        } else {
            null
        }
    }

    override fun authenticate(route: Route?, response: Response): Request? {
        synchronized(this) {
            val responseBody = response.body().toString()
            val refreshToken = preference.refreshToken

            val token = when {
                responseBody.contains("token provided has expired") -> null
                refreshToken == "" -> null
                else -> try {
                    requestNewAccessToken()
                } catch (e: Throwable) {
                    e.printStackTrace()
                    null
                }
            }
            return if (token == null) {
                preference.cleanAuthorizationData()
                tokenHasExpired.invoke()
                throw IOException(responseBody)
            } else {
                response.request()
                    .newBuilder()
                    .header("Authorization", "Bearer $token")
                    .build()
            }
        }
    }


}
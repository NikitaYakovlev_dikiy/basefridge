package webant.ru.basefridge.core.model

import com.google.firebase.database.Exclude

class ProductVersionModel(
    override var versionNumber: Long? = null
) : ProductVersion

data class ProductModel(
    @get:Exclude
    override var id: Long? = null,
    override var baseHeft: Long? = null,
    override var name: String? = null
) : Product

class FridgeModel(
    @get:Exclude
    override var id: Long? = null,
    override var products: ArrayList<FridgeProduct>? = null,
    override var setting: Setting? = null
) : Fridge

class SettingModel(
    @get:Exclude
    override var id: Long? = null,
    override var lat: Double? = null,
    override var long: Double? = null,
    override var name: String? = null,
    override var users: ArrayList<User>? = null
) : Setting

class FridgeProductModel(
    @get:Exclude
    override var id: Long? = null,
    override var count: Long? = null,
    override var data: String? = null
) : FridgeProduct

class UserModel(
    @get:Exclude
    override var id: Long? = null,
    override var mode: Long? = null
) : User
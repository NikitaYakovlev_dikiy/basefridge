package webant.ru.basefridge.core.database

import io.reactivex.Completable
import io.reactivex.Single
import io.realm.RealmObject
import io.realm.RealmQuery
import webant.ru.basefridge.core.model.BaseModel

interface DataBaseGateway {
    fun <T> createOrUpdate(model: T): Completable

    fun <T : RealmObject> getAll(
        requestType: Class<T>,
        query: ((RealmQuery<T>) -> RealmQuery<T>)? = null
    ): Single<ArrayList<T>>

    fun <T : RealmObject, OutType : BaseModel> getAll(
        requestType: Class<T>,
        outType: Class<OutType>,
        query: ((RealmQuery<T>) -> RealmQuery<T>)? = null
    ): Single<ArrayList<OutType>>


    fun <T : RealmObject> deleteAll(requestType: Class<T>, query: ((RealmQuery<T>) -> RealmQuery<T>)? = null): Completable
}

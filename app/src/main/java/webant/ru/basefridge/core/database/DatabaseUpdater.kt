package webant.ru.basefridge.core.database

import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import webant.ru.basefridge.App
import webant.ru.basefridge.core.firebase.FireBaseGateway
import webant.ru.basefridge.core.firebase.getAll
import webant.ru.basefridge.core.model.ProductModel
import webant.ru.basefridge.core.model.ProductVersion
import webant.ru.basefridge.core.model.SettingModel
import webant.ru.basefridge.core.model.UserModel
import javax.inject.Inject

class DatabaseUpdater {
    @Inject
    lateinit var fireBase: FireBaseGateway
    @Inject
    lateinit var database: DataBaseGateway
    private val composeDisposable = CompositeDisposable()

    init {
        App.appComponent.inject(this)
//        getFridge(1)
        database.getAll(RealmProduct::class.java, ProductModel::class.java)
            .subscribe({
                it
            }, {
                it
            }).addToComposite()
        updateProducts()
    }

    fun update() {

        checkVersion()
    }

    fun changeSetting() {
        val setting = SettingModel(1, 0.2, 0.2, "fridge2", ArrayList())
        setting.users?.add(UserModel(1, 0))
        fireBase.changeSetting(setting)
            .subscribe({
                "21"
            }, {
                it
            }).addToComposite()
    }

    fun addProduct() {
        val model = ProductModel(3, 1, "name")
        fireBase.changeProduct(model)
            .subscribe({
                model.saveToBd(database)
            }, {
                it
            }).addToComposite()
    }

    fun checkVersion(): Single<ProductVersion> {
        return fireBase.getVersion()
    }

    fun updateProducts() {
        getAll { fireBase.getProduct(it) }
            .subscribe({
                it.forEach {
                    it.saveToBd(database).subscribe()
                }
            }, {
                it
            }).addToComposite()
    }

    fun getFridge(fridgeId: Long) {
        fireBase.getFridge(fridgeId, 1)
            .subscribe({
                it.forEach {
                    it.saveToBd(database)
                }
            }, {
                it
            }).addToComposite()
    }

    fun getFridgeProduct(fridgeId: Long) {
        getAll { fireBase.getFridgeProduct(fridgeId, it) }
            .subscribe({
                it
            }, {
                it.printStackTrace()
            }).addToComposite()
    }

    private fun Disposable.addToComposite() {
        composeDisposable.add(this)
    }

}
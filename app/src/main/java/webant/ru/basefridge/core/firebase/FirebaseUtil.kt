package webant.ru.basefridge.core.firebase

import com.androidhuman.rxfirebase2.database.data
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseReference
import io.reactivex.Single
import webant.ru.basefridge.core.model.IdBaseModel

fun DatabaseReference.startAtKey(start: Long, limit: Int, nextStart: ((Long) -> Unit)? = null): Single<DataSnapshot> =
    this.orderByKey()
        .startAt(start.toString())
        .limitToFirst(limit)
        .data()
        .map { dataSnapshot ->
            if (nextStart != null) {
                var maxKey = dataSnapshot.children.maxBy { it.key?.toLong() ?: 0 }?.key?.toLong()
                if (maxKey != null) {
                    maxKey++
                }
                nextStart.invoke(maxKey ?: Long.MAX_VALUE)
            }
            dataSnapshot
        }

fun <T : IdBaseModel> getAll(request: (Long) -> Single<ArrayList<T>>): Single<ArrayList<T>> {
    var hasChildren = true
    var last = 0L
    val products = ArrayList<T>()
    return Single.defer {
        return@defer request.invoke(last)
            .map { list ->
                hasChildren = list.size == FireBase.LIMIT
                if (list.isNotEmpty()) {
                    last = list.maxBy { it.id ?: 0L }?.id ?: 0L
                    last++
                    products.addAll(list)
                }
            }
    }
        .repeatUntil { !hasChildren }
        .lastOrError()
        .map { products }
}
package webant.ru.basefridge.core

import dagger.Module
import dagger.Provides
import webant.ru.basefridge.core.database.DataBaseGateway
import webant.ru.basefridge.core.database.Database
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(): DataBaseGateway = Database()
}
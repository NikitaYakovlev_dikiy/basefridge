package webant.ru.basefridge.core.Preference

import android.content.Context
import ru.webant.base.core.shared.BasePreferenceWorker

object Preference {
    lateinit var preference: PreferenceWorker

    fun init(context: Context) {
        preference = PreferenceWorker(context)
    }

    class PreferenceWorker(context: Context) : BasePreferenceWorker(context) {
        var name = PreferenceValue<String>("name").value
        var fridgeId = PreferenceValue<String>("fridgeId").value
    }
}
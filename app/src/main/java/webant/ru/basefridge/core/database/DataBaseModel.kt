package webant.ru.basefridge.core.database

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import webant.ru.basefridge.core.model.*

open class RealmProductVersion(
    override var versionNumber: Long? = null
) : RealmObject(),
    ProductVersion

open class RealmProduct(
    @PrimaryKey
    override var id: Long? = null,
    override var baseHeft: Long? = null,
    override var name: String? = null
) : RealmObject(), Product

open class RealmFridge(
    @PrimaryKey
    override var id: Long? = null,
    @Ignore
    override var products: ArrayList<FridgeProduct>? = null,
    @Ignore
    override var setting: Setting? = null,
    var bdProducts: RealmList<RealmFridgeProduct>? = products?.toRealmList(),
    var bdSetting: RealmSetting? = setting?.realmModel()
) : RealmObject(), Fridge

open class RealmFridgeProduct(
    @PrimaryKey
    override var id: Long? = null,
    override var count: Long? = null,
    override var data: String? = null
) : RealmObject(), FridgeProduct

open class RealmSetting(
    @PrimaryKey
    override var id: Long? = null,
    override var lat: Double? = null,
    override var long: Double? = null,
    override var name: String? = null,
    @Ignore
    override var users: ArrayList<User>? = null,
    var bdUser: RealmList<RealmUser>? = users?.toRealmList()
) : RealmObject(), Setting

open class RealmUser(
    @PrimaryKey
    override var id: Long? = null,
    override var mode: Long? = null
) : RealmObject(), User
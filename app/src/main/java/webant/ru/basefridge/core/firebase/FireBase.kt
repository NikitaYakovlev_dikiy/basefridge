package webant.ru.basefridge.core.firebase

import com.androidhuman.rxfirebase2.database.data
import com.androidhuman.rxfirebase2.database.rxSetValue
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import io.reactivex.Single
import webant.ru.basefridge.core.model.*

class FireBase(private var database: FirebaseDatabase) : FireBaseGateway {

    override fun changeProductToFridge(fridgeId: Long, product: FridgeProductModel) =
        reference(FRIDGE, fridgeId.toString(), PRODUCTS, product.id.toString())
            .rxSetValue(product)

    override fun changeSetting(setting: SettingModel) =
        reference(FRIDGE, setting.id.toString(), SETTING)
            .rxSetValue(setting)

    override fun getUserFromFridge(fridgeId: Long, startAt: Long) =
        request(startAt, FRIDGE, fridgeId.toString(), SETTING, USERS)
            .map {
                val items = ArrayList<User>()
                it.children.forEach { items.add(UserModel().apply { fromSnapshot(it) }) }
                items
            }

    override fun setVersion(version: Long) =
        reference(PRODUCT, VERSION)
            .rxSetValue(version)

    override fun changeProduct(product: ProductModel) =
        reference(PRODUCT, PRODUCTS, product.id.toString())
            .rxSetValue(product)

    override fun getProduct(startAt: Long) =
        request(startAt, PRODUCT, PRODUCTS)
            .map {
                val products = ArrayList<Product>()
                it.children.forEach { products.add(ProductModel().apply { fromSnapshot(it) }) }
                products
            }

    override fun getVersion(): Single<ProductVersion> =
        reference(PRODUCT)
            .data()
            .map { ProductVersionModel().apply { fromSnapshot(it) } }

    override fun getFridge(startAt: Long, limit: Int?) =
        request(startAt, FRIDGE, limit = limit)
            .map {
                val fridgeList = ArrayList<Fridge>()
                it.children.forEach { fridgeList.add(FridgeModel().apply { fromSnapshot(it) }) }
                fridgeList
            }

    override fun getFridgeProduct(fridgeId: Long, startAt: Long) =
        request(startAt, FRIDGE, fridgeId.toString(), PRODUCTS)
            .map {
                val products = ArrayList<FridgeProduct>()
                it.children.forEach { products.add(FridgeProductModel().apply { fromSnapshot(it) }) }
                products
            }

    override fun getFridgeUser(fridgeId: Long, startAt: Long) =
        request(startAt, FRIDGE, fridgeId.toString(), SETTING)
            .map {
                val products = ArrayList<User>()
                it.children.forEach { products.add(UserModel().apply { fromSnapshot(it) }) }
                products
            }

    private fun reference(vararg path: String): DatabaseReference {
        var reference = database.getReference(path.first())
        path.forEachIndexed { index, s ->
            if (index != 0) {
                reference = reference.child(s)
            }
        }
        reference.keepSynced(true)
        return reference
    }

    private fun request(startAt: Long, vararg path: String, limit: Int? = null): Single<DataSnapshot> {
        val validLimit = limit ?: LIMIT
        return reference(*path).startAtKey(startAt, validLimit)
    }

    companion object {
        const val LIMIT = 3
        const val VERSION = "version"
        const val SETTING = "setting"
        const val FRIDGE = "fridge"
        const val PRODUCT = "product"
        const val PRODUCTS = "products"
        const val USERS = "users"
    }
}
package webant.ru.basefridge.core

import dagger.Component
import webant.ru.basefridge.core.database.DatabaseUpdater
import javax.inject.Singleton


@Singleton
@Component(modules = [(ApplicationModule::class), (DatabaseModule::class)])
interface AppComponent {
    fun inject(target: DatabaseUpdater)
}
package webant.ru.basefridge.core.firebase

import io.reactivex.Completable
import io.reactivex.Single
import webant.ru.basefridge.core.model.*

interface FireBaseGateway {
    fun getVersion(): Single<ProductVersion>

    fun getProduct(startAt: Long): Single<ArrayList<Product>>

    fun getFridge(startAt: Long, limit: Int? = null): Single<ArrayList<Fridge>>

    fun getFridgeProduct(fridgeId: Long, startAt: Long): Single<ArrayList<FridgeProduct>>

    fun getFridgeUser(fridgeId: Long, startAt: Long): Single<ArrayList<User>>

    fun setVersion(version: Long): Completable

    fun changeProduct(product: ProductModel): Completable

    fun changeProductToFridge(fridgeId: Long, product: FridgeProductModel): Completable

    fun changeSetting(setting: SettingModel): Completable

    fun getUserFromFridge(fridgeId: Long, startAt: Long): Single<ArrayList<User>>
}
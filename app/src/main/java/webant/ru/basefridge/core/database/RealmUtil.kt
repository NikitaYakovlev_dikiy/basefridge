package webant.ru.basefridge.core.database

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.RealmResults
import webant.ru.basefridge.core.model.BaseModel

@Suppress("UNCHECKED_CAST")
fun <inType : BaseModel, outType : BaseModel> ArrayList<inType>.toRealmList(): RealmList<outType> {
    val realmList = RealmList<outType>()
    this.forEach {
        val realmModel = it.realmModel() as? outType
        if (realmModel != null) {
            realmList.add(realmModel)
        }
    }
    return realmList
}

@Suppress("UNCHECKED_CAST")
fun <inType : BaseModel, outType : BaseModel> RealmList<inType>.toBaseArrayList(): ArrayList<outType> {
    val realmList = ArrayList<outType>()
    this.forEach {
        realmList.add((it as outType))
    }
    return realmList
}

@Suppress("UNCHECKED_CAST")
fun <inType : RealmObject, outType : BaseModel> RealmResults<inType>.toBaseArrayList(): ArrayList<outType> {
    val realmList = ArrayList<outType>()
    this.forEach {
        realmList.add((it as outType))
    }
    return realmList
}

fun <T> RealmResults<T>.toArrayList() =
    ArrayList<T>().apply {
        this@toArrayList.forEach {
            if (it != null) {
                this.add(it)
            }
        }
    }

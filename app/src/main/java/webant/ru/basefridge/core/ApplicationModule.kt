package webant.ru.basefridge.core

import com.google.firebase.database.FirebaseDatabase
import dagger.Module
import dagger.Provides
import webant.ru.basefridge.App
import webant.ru.basefridge.core.firebase.FireBase
import webant.ru.basefridge.core.firebase.FireBaseGateway
import javax.inject.Singleton

@Module
class ApplicationModule(application: App) {
    var application: App
        @Provides
        @Singleton
        get() = field

    init {
        this.application = application
    }

    @Provides
    @Singleton
    fun provideFirebaseGateway(): FireBaseGateway = FireBase(FirebaseDatabase.getInstance())
}
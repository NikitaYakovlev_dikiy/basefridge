package webant.ru.basefridge.core.model

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.Exclude
import webant.ru.basefridge.core.database.*
import webant.ru.basefridge.core.model.FireBaseField.BASE_HEFT
import webant.ru.basefridge.core.model.FireBaseField.COUNT
import webant.ru.basefridge.core.model.FireBaseField.DATE
import webant.ru.basefridge.core.model.FireBaseField.LAT
import webant.ru.basefridge.core.model.FireBaseField.LONG
import webant.ru.basefridge.core.model.FireBaseField.MODE
import webant.ru.basefridge.core.model.FireBaseField.NAME
import webant.ru.basefridge.core.model.FireBaseField.PRODUCTS
import webant.ru.basefridge.core.model.FireBaseField.SETTING
import webant.ru.basefridge.core.model.FireBaseField.USERS
import java.io.Serializable

interface BaseModel : Serializable {
    fun baseModel(): BaseModel
    fun realmModel(): BaseModel
    fun fromSnapshot(data: DataSnapshot)
    fun saveToBd(database: DataBaseGateway) =
        database.createOrUpdate(realmModel())

    fun fromRealm(database: DataBaseGateway) {

    }
}

interface IdBaseModel : BaseModel {
    @get:Exclude
    var id: Long?
}

interface Product : IdBaseModel {
    var baseHeft: Long?
    var name: String?

    override fun baseModel() = ProductModel(id, baseHeft, name)
    override fun realmModel() = RealmProduct(id, baseHeft, name)
    override fun fromSnapshot(data: DataSnapshot) {
        id = data.key?.toLong()
        baseHeft = data.child(BASE_HEFT).value as? Long
        name = data.child(NAME).value as? String
    }
}

interface ProductVersion : BaseModel {
    var versionNumber: Long?

    override fun baseModel() = ProductVersionModel(versionNumber)
    override fun realmModel() = RealmProductVersion(versionNumber)
    override fun fromSnapshot(data: DataSnapshot) {
        versionNumber = data.value as? Long
    }
}

interface Fridge : IdBaseModel {
    var products: ArrayList<FridgeProduct>?
    var setting: Setting?

    override fun baseModel() = FridgeModel(id, products, setting)
    override fun realmModel() = RealmFridge(id, products, setting)
    override fun fromSnapshot(data: DataSnapshot) {
        id = data.key?.toLong()
        products = ArrayList()
        data.child(PRODUCTS).children.forEach { products?.add(FridgeProductModel().apply { fromSnapshot(it) }) }
        setting = SettingModel().apply { fromSnapshot(data.child(SETTING)) }
    }
}

interface FridgeProduct : IdBaseModel {
    var count: Long?
    var data: String?

    override fun baseModel() = FridgeProductModel(id, count, data)
    override fun realmModel() = RealmFridgeProduct(id, count, data)
    override fun fromSnapshot(data: DataSnapshot) {
        id = data.key?.toLong()
        count = data.child(COUNT).value as? Long
        this.data = data.child(DATE).value as? String
    }
}

interface Setting : IdBaseModel {
    var lat: Double?
    var long: Double?
    var name: String?
    var users: ArrayList<User>?

    override fun baseModel() = SettingModel(id, lat, long, name, users)
    override fun realmModel() = RealmSetting(id, lat, long, name, users)
    override fun fromSnapshot(data: DataSnapshot) {
        lat = data.child(LAT).value as? Double
        long = data.child(LONG).value as? Double
        name = data.child(NAME).value as? String
        users = ArrayList()
        data.child(USERS).children.forEach { users?.add(UserModel().apply { fromSnapshot(it) }) }
    }
}

interface User : IdBaseModel {
    var mode: Long?
    override fun baseModel() = UserModel(id, mode)
    override fun realmModel() = RealmUser(id, mode)
    override fun fromSnapshot(data: DataSnapshot) {
        id = data.key?.toLong()
        mode = data.child(MODE).value as Long
    }
}

object FireBaseField {
    const val LAT = "lat"
    const val LONG = "long"
    const val SETTING = "setting"
    const val DATE = "date"
    const val COUNT = "count"
    const val PRODUCTS = "products"
    const val NAME = "name"
    const val USERS = "users"
    const val BASE_HEFT = "baseHeft"
    const val MODE = "mode"
}


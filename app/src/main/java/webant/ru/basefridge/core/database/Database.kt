package webant.ru.basefridge.core.database

import io.reactivex.Completable
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmObject
import io.realm.RealmQuery
import webant.ru.basefridge.core.model.BaseModel

open class Database : DataBaseGateway {
    override fun <T : RealmObject> getAll(
        requestType: Class<T>,
        query: ((RealmQuery<T>) -> RealmQuery<T>)?
    ): Single<ArrayList<T>> {
        return Single.create { emitter ->
            realm.executeTransactionAsync(
                { realm ->
                    var realmQuery = realm.where(requestType)
                    if (query != null)
                        realmQuery = query.invoke(realmQuery)
                    val model = realmQuery.findAll()
                    emitter.onSuccess(model.toArrayList())
                }, null,
                emitter::onError
            )
        }
    }

    open val realm: Realm by lazy { Realm.getDefaultInstance() }

    override fun <T : RealmObject, OutType : BaseModel> getAll(
        requestType: Class<T>,
        outType: Class<OutType>,
        query: ((RealmQuery<T>) -> RealmQuery<T>)?
    ): Single<ArrayList<OutType>> {
        return Single.create { emitter ->
            realm.executeTransactionAsync(
                { realm ->
                    var realmQuery = realm.where(requestType)
                    if (query != null) {
                        realmQuery = query.invoke(realmQuery)
                    }
                    val model = realmQuery.findAll()

                    try {
                        emitter.onSuccess(model.toBaseArrayList())
                    } catch (e: Throwable) {
                        emitter.onError(Throwable("is not a BaseModel"))
                    }
                }, null, emitter::onError
            )
        }
    }

    override fun <T> createOrUpdate(model: T): Completable {
        return Completable.create { emitter ->
            if (model is RealmObject) {
                realm.executeTransactionAsync(
                    { it.copyToRealmOrUpdate(model as RealmObject) },
                    emitter::onComplete,
                    emitter::onError
                )
            } else {
                emitter.onError(Throwable("is not a RealmObject"))
            }
        }.onErrorResumeNext { null }
    }

    override fun <T : RealmObject> deleteAll(
        requestType: Class<T>,
        query: ((RealmQuery<T>) -> RealmQuery<T>)?
    ): Completable {
        return Completable.create { emitter ->
            realm.executeTransactionAsync(
                { realm ->
                    var realmQuery = realm.where(requestType)
                    if (query != null) {
                        realmQuery = query.invoke(realmQuery)
                    }
                    realmQuery.findAll().deleteAllFromRealm()
                }, emitter::onComplete, emitter::onError
            )
        }.onErrorResumeNext { null }
    }
}

package webant.ru.basefridge

import android.app.Application
import com.google.firebase.FirebaseApp
import io.realm.Realm
import io.realm.RealmConfiguration
import webant.ru.basefridge.core.AppComponent
import webant.ru.basefridge.core.ApplicationModule
import webant.ru.basefridge.core.DaggerAppComponent
import webant.ru.basefridge.core.Preference.Preference
import webant.ru.basefridge.core.database.DatabaseUpdater

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Preference.init(this)

        FirebaseApp.initializeApp(this)

        Realm.init(this)
        Realm.setDefaultConfiguration(
            RealmConfiguration.Builder()
                .name("database")
                .deleteRealmIfMigrationNeeded()
                .build()
        )

        appComponent = DaggerAppComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()

        databaseUpdater = DatabaseUpdater()
    }

    companion object {
        lateinit var appComponent: AppComponent
        lateinit var databaseUpdater: DatabaseUpdater
    }
}
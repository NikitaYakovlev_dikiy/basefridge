package ru.webant.base.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class BaseFragment : Fragment() {
    abstract val fragmentId: Int
    abstract val fragmentTag: String
    abstract var layoutId: Int
    private var isActionExit = true

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(layoutId, container, false)

    override fun onResume() {
        isActionExit = true
        super.onResume()
    }

    override fun onDestroy() {
        if (isActionExit)
            exitFromActivity()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        isActionExit = false
        super.onSaveInstanceState(outState)
    }

    abstract fun exitFromActivity()
}
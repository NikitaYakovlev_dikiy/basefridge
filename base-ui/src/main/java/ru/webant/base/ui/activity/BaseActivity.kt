package ru.webant.base.ui.activity

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {
    abstract var layoutId: Int
    var fragmentViewId: Int = 0
    var fragmentOpenAnimationId: Int = 0
    var fragmentCloseAnimationId: Int = 0
    private var isActionExit = false
    private var waiterPermissionMap = HashMap<Int, ((success: Boolean, neverShow: Boolean) -> Unit)?>()

    val fragmentTransaction
        get() = supportFragmentManager.beginTransaction().apply {
            setCustomAnimations(fragmentOpenAnimationId, fragmentCloseAnimationId)
        }

    override fun onDestroy() {
        if (isActionExit)
            exitFromActivity()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        isActionExit = false
        super.onSaveInstanceState(outState)
    }

    final override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(layoutId)
        super.onCreate(savedInstanceState)
        init()
    }

    abstract fun init()
    override fun onResume() {
        isActionExit = true
        super.onResume()
    }

    open fun isHavePermissions(permissions: Array<String>): Boolean {
        val permission =
            permissions.firstOrNull { ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED }
        return permission == null
    }

    open fun requestPermissions(
        permission: Array<String>,
        doAfter: ((success: Boolean, neverShow: Boolean) -> Unit)? = null
    ) {
        var requestCode = waiterPermissionMap.maxBy { it.key }?.key ?: 0
        requestCode++
        waiterPermissionMap[requestCode] = doAfter

        if (!isHavePermissions(permission)) {
            ActivityCompat.requestPermissions(this, permission, requestCode)
        } else {
            doAfter?.invoke(true, false)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var havePermissions = true
        var userClickToNeverShow = false
        for (permission in permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                havePermissions = false
                break
            } else {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    havePermissions = false
                    userClickToNeverShow = true
                    break
                }
            }
        }
        waiterPermissionMap[requestCode]?.invoke(havePermissions, userClickToNeverShow)
    }

    abstract fun exitFromActivity()
}
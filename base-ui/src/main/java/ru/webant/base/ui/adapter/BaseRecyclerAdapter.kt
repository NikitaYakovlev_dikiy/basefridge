package ru.webant.base.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class BaseRecyclerAdapter<T> :
    RecyclerView.Adapter<BaseRecyclerAdapter<T>.ViewHolder>(), PaginationGateway<T> {

    abstract var items: ArrayList<T>
    abstract var itemResId: Int
    abstract var paginationResId: Int
    var turnOnPagination = false
    var progressPagination = false
    var paginationRequest: (() -> Unit)? = null


    override fun linkItems(): ArrayList<T> = items

    override fun requestNext(request: () -> Unit) {
        turnOnPagination = true
        paginationRequest = request
    }

    override fun onUpdate(progress: Float) {
        val isEnd = progress >= 1
        if (isEnd) {
            turnOnPagination = false
        } else {
            progressPagination = false
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        val viewResId = if (type == PAGINATION_TYPE) {
            paginationResId
        } else {
            itemResId
        }
        return ViewHolder(LayoutInflater.from(parent.context).inflate(viewResId, parent, false))
    }

    override fun getItemCount(): Int {
        var size = items.size
        if (turnOnPagination)
            size++
        return size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items.getOrNull(position), position)

    override fun getItemViewType(position: Int): Int {
        return if (position >= items.size) {
            PAGINATION_TYPE
        } else {
            super.getItemViewType(position)
        }
    }

    abstract fun bindView(baseItem: T, position: Int, view: View)

    abstract fun endList(position: Int, view: View)

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(baseItem: T?, position: Int) {
            if (baseItem != null) {
                bindView(baseItem, position, view)
            } else {
                if (!progressPagination) {
                    progressPagination = true
                    paginationRequest?.invoke()
                    endList(position, view)
                }
            }
        }
    }

    companion object {
        const val PAGINATION_TYPE = 1293
    }
}
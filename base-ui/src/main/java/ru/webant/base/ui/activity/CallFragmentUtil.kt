package ru.webant.base.ui.activity

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import ru.webant.base.ui.fragment.BaseFragment

fun BaseActivity.startFragment(fragment: BaseFragment, closeOther: Boolean = false) {
    startFragment(fragmentViewId, fragment, fragmentTransaction, fragment.fragmentTag, closeOther)
}

fun AppCompatActivity.startFragment(
    viewId: Int,
    fragment: Fragment,
    fragmentTransaction: FragmentTransaction? = null,
    fragmentTag: String? = null,
    closeOther: Boolean = false
) {
    if (closeOther)
        closeAllFragment()

    this.closeKeyBoard()

    (fragmentTransaction ?: supportFragmentManager.beginTransaction()).apply {
        addToBackStack(fragmentTag)
        replace(viewId, fragment)
        commit()
    }
}

fun AppCompatActivity.closeAllFragment(fragmentTransaction: FragmentTransaction? = null) {
    val fragmentList = supportFragmentManager!!.fragments
    (fragmentTransaction ?: supportFragmentManager.beginTransaction()).apply {
        for (fragment in fragmentList)
            remove(fragment)
        commit()
    }
}

fun FragmentActivity.closeAllFragment(fragmentTransaction: FragmentTransaction? = null) {
    (this as AppCompatActivity).closeAllFragment(fragmentTransaction)
}

fun FragmentActivity.startFragment(
    viewId: Int,
    fragment: Fragment,
    fragmentTransaction: FragmentTransaction? = null,
    fragmentTag: String? = null,
    closeOther: Boolean = false
) {
    (this as AppCompatActivity).startFragment(viewId, fragment, fragmentTransaction, fragmentTag, closeOther)
}
package ru.webant.base.ui.dialog

import android.content.Context
import android.support.v7.app.AlertDialog

abstract class BaseDialog(context: Context) : AlertDialog(context) {
    abstract val layoutId: Int

    init {
        @Suppress("LeakingThis")
        Builder(context)
            .setView(layoutId)
            .create()
            .apply {
                init(this)
            }
            .show()
    }

    abstract fun init(alertDialog: AlertDialog)
}
package ru.webant.base.ui.view

import android.app.Activity
import android.content.Context
import android.util.DisplayMetrics
import android.view.View

fun View.hide(gone: Boolean = true) {
    this.visibility = if (gone) {
        View.GONE
    } else {
        View.INVISIBLE
    }
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun Context.pxToDp(px: Float): Float = px / resources.displayMetrics.density

fun Context.dpToPx(dp: Float): Float = dp * resources.displayMetrics.density

fun Activity.getScreenWidth(): Int {
    val metrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(metrics)
    return metrics.widthPixels
}

fun Activity.getScreenHeight(): Int {
    val metrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(metrics)
    return metrics.heightPixels
}
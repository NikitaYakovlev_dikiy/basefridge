package ru.webant.base.ui.adapter

interface PaginationGateway<T> {
    fun linkItems(): ArrayList<T>
    fun requestNext(request: () -> Unit)
    fun onUpdate(progress: Float)
    fun onError(t: Throwable)
}
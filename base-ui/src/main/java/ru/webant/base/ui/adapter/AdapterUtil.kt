package ru.webant.base.ui.adapter

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner

fun Spinner.initSpinner(
    items: Array<String>,
    spinnerRes: Int = android.R.layout.simple_spinner_dropdown_item,
    onSelect: (String) -> Unit
) {
    val adapter = ArrayAdapter(
        context,
        spinnerRes,
        items
    )

    adapter.setDropDownViewResource(spinnerRes)
    this.adapter = adapter
    onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {}
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            onSelect.invoke(items[position])
        }
    }
}
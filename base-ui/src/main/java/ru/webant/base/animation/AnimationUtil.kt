package ru.webant.base.animation

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.TimeInterpolator
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.view.View

fun View.translationAnimation(
    isHide: Boolean,
    duration: Long = 200L,
    orientation: AnimationOrientation = AnimationOrientation.BOTTOM,
    interpolator: TimeInterpolator = LinearOutSlowInInterpolator(),
    customValue: Float? = null
): AnimatorSet {
    val animator = AnimatorSet()
    val property = when (orientation) {
        AnimationOrientation.TOP, AnimationOrientation.BOTTOM -> "translationY"
        else -> "translationX"
    }
    var value = customValue ?: when (orientation) {
        AnimationOrientation.RIGHT, AnimationOrientation.LEFT -> this.width.toFloat()
        else -> this.height.toFloat()
    }

    if (orientation == AnimationOrientation.TOP || orientation == AnimationOrientation.LEFT) {
        value = -value
    }

    var start = 0F
    var end = 0F
    if (isHide) {
        end = value
    } else {
        start = value
    }

    animator.playTogether(ObjectAnimator.ofFloat(this, property, start, end))
    animator.duration = duration
    animator.interpolator = interpolator
    return animator
}

fun Animator.onEndListener(onEnd: () -> Unit): Animator {
    this.addListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(p0: Animator?) = Unit

        override fun onAnimationEnd(p0: Animator?) = onEnd.invoke()

        override fun onAnimationCancel(p0: Animator?) = Unit

        override fun onAnimationStart(p0: Animator?) = Unit
    })
    return this
}

fun Animator.onStartListener(onStart: () -> Unit): Animator {
    this.addListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(p0: Animator?) = Unit

        override fun onAnimationEnd(p0: Animator?) = Unit

        override fun onAnimationCancel(p0: Animator?) = Unit

        override fun onAnimationStart(p0: Animator?) = onStart.invoke()
    })
    return this
}


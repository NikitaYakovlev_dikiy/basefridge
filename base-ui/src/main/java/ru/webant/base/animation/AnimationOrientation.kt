package ru.webant.base.animation

enum class AnimationOrientation {
    TOP,BOTTOM,LEFT,RIGHT
}